<?php


namespace app\Controllers;

use sys\Validation\Validator;
use sys\Http\Redirect;

class Controller {


    public function validate($data, $rules)
    {
        //return json_decode($data);
        $valid = new Validator;
        $check = $valid->check($data, $rules);

        if($check === false)
        {
           $errors = $valid->errors();

            $url = $_SERVER['REQUEST_URI'];

           return Redirect::to($url, $errors );

        }

        return false;



    }

	
}

?>