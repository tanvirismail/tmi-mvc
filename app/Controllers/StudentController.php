<?php


namespace app\Controllers;

use sys\Database\RAWQUERY;
use sys\Storage;
use sys\Views;

class StudentController extends Controller
{
	
	public function index()
	{
        $student = RAWQUERY::select("SELECT * FROM `test`");
        return Views::this('student/home', compact('student'));

	}
	
	public function show($data)
	{
		$pp = 'show';
		$tt = 'tanvir';
		$ff = ['id'=>'56','name'=>'tanvir'];
		return view('student/show', compact('tt', 'ff', 'pp') );
		
	}
	
	public function store($data)
	{
	
		$name = $data->name;
		$mobile = $data->mobile;
		$storeFile = Storage::putFile('tanvir', $data->file->image); // same name existing file, now rename
		//$storeFile = Storage::saveAs('tanvir', 'rename', $data->file->image); // rename file
		//$storeFile = Storage::save('tanvir', $data->file->image); // replace existing file
		
		$image = $storeFile;
		
		$insert = RAWQUERY::insert("INSERT INTO `test`(name, mobile, image) VALUES('$name', '$mobile', '$image')");
		
		return redirect('/student', 'successfull');
		
	}
	
	public function edit($data)
	{
		$student = RAWQUERY::select("SELECT * FROM `test` WHERE `id` = $data->id");
		return view('student/edit', compact('student'));
		
	}
	
	public function update($data)
	{

        if($data->file->image->tmp_name){
            Storage::unLink('tanvir', $data->oldImage);
            $storeFile = Storage::putFile('tanvir', $data->file->image);
            $image = $storeFile;
        } else {
            $image = $data->oldImage;
        }

		$update = RAWQUERY::update("UPDATE `test` SET name='$data->name', mobile='$data->mobile', image='$image' WHERE `id` = $data->id");

		return redirect('/student', 'successfull');
	}
	
	public function delete($data)
	{
		$delete = RAWQUERY::delete("DELETE FROM `test` WHERE `id`='$data->id'");
		return redirect('/student', 'successfull');
		
	}
	
	
	
}








