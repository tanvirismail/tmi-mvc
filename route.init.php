<?php
use sys\Route;

Route::get('/', 'Home@index');
Route::get('/login', 'UserController@login');
Route::post('/login', 'UserController@logedin');
Route::get('/register', 'UserController@register');
Route::post('/register', 'UserController@store');



Route::get('/test', 'test@index');
Route::get('/test/{id}', 'test@show');
Route::get('/test/edit/{id}', 'test@edit');
Route::post('/test', 'test@store');
Route::put('/test/{id}', 'test@update');
Route::delete('/test/{id}', 'test@delete');

Route::get('/student', 'StudentController@index');
Route::get('/student/{id}', 'StudentController@show');
Route::get('/student/edit/{id}', 'StudentController@edit');
Route::post('/student', 'StudentController@store');
Route::put('/student/{id}', 'StudentController@update');
Route::delete('/student/{id}', 'StudentController@delete');


