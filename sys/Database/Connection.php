<?php

namespace sys\Database;
use PDO;
class Connection extends PDO {
	
	private $conn;
	
	public function __construct(){
		
		$dns = ENGINE .':dbname='. DATABASE .";host=". HOST ; 
		$username = USER;
		$password = PASSWORD;
		
		if (!isset($this->conn)){
			try {
				$this->conn = parent::__construct($dns, $username, $password);
			} catch (PDOException $e) {
				die("Failed to connect Database : ".$e->getMessage());
			}
		}
		return $this->conn;
	}

	
}

?>