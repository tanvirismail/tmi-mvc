<?php

namespace sys\Database;


use sys\Database\QUERY;
use Exception;

class DB extends QUERY
{

    private static $select;
    private static $table;
    private static $where = array();

    public function select($column = '*')
    {
        $sql = '';
        if( is_array($column) )
        {
            foreach ($column as $value)
            {
                $newValue = array();
                $value = explode('.', $value);
                foreach( $value as $v )
                {
                    $newValue[] = '`'.$v.'`';
                }
                $value = implode('.', $newValue);
                $sql .= $value . ', ';
            }
            $sql = rtrim($sql, ', ');
        }
        elseif( !is_array($column) ) {

            $column = explode('.', $column);
            $newValue = [];
            foreach( $column as $v )
            {
                if( $v == '*' )
                { $newValue[] = $v; }
                else
                { $newValue[] = '`'.$v.'`'; }
            }
            $sql = implode('.', $newValue);
        }


        self::$select =  $sql;
        return new self;
    }

    public function table($name)
    {
        self::$table = '`'.$name.'`';
        return new self;
    }

    public function where($condition)
    {

        if( is_array($condition[0]) )
        {
            $where = [];
            foreach ($condition as $item) {
                if(count($item) == '3') {
                    $tblCol = trim($item[0]);
                    $operator = trim($item[1]);
                    $value = trim($item[2]);

                    $tblCol = explode('.', $tblCol);
                    $bindValue = (@$tblCol[1]) ? ':'.trim($tblCol[1]) : ':'.trim($tblCol[0]);
                    self::$where['value'][$bindValue] = $value;

                    $newItem = [];
                    foreach ($tblCol as $item) {
                        $newItem[] = '`' . $item . '`';
                    }
                    $tblCol = implode('.', $newItem);
                    $opetators = ['=', '>', '<', '>=', '<='];
                    if (in_array($operator, $opetators)) {
                        $where[] = $tblCol .' '. $operator .' '. $bindValue;
                    } else {
                        try{
                            throw new Exception("Invalid operator sign in ");
                        }catch(Exception $e){
                            $getTrace = $e->getTrace();
                            echo  $e->getMessage().' <b>'.$getTrace[0]['function']. '</b> function. ' .$e->getTraceAsString();
                            exit();
                        }
                    }
                }
            }
            self::$where['sql'] = implode(' AND ', $where);

        }
        elseif( is_array($condition) ) {
            if(count($condition)=='3')
            {
                $tblCol = trim($condition[0]);
                $operator = trim($condition[1]);
                $value = trim($condition[2]);

                $tblCol = explode('.', $tblCol);
                $bindValue = (@$tblCol[1]) ? ':'.$tblCol[1] : ':'.$tblCol[0];
                $bindValue = (@$tblCol[1]) ? ':'.trim($tblCol[1]) : ':'.trim($tblCol[0]);
                self::$where['value'][$bindValue] = $value;
                foreach ($tblCol as $item) {
                    @$newItem[] = '`' . trim($item) . '`';
                }
                $tblCol = implode('.', $newItem);
                $opetators = ['=', '>', '<', '>=', '<='];
                if (in_array($operator, $opetators))
                {
                    self::$where['sql'] = $tblCol .' '. $operator .' '. $bindValue;
                } else {
                    try{
                        throw new Exception("Invalid operator sign in ");
                    }catch(Exception $e){
                        $getTrace = $e->getTrace();
                        echo  $e->getMessage().' <b>'.$getTrace[0]['function']. '</b> function. ' .$e->getTraceAsString();
                        exit();
                    }
                }
            } else {
                try{
                    throw new Exception("3 parameters parse but ");
                }catch(Exception $e){
                    $getTrace = $e->getTrace();
                    $passParam = count($getTrace[0]['args'][0]);
                    echo  $e->getMessage(). $passParam .' given in <b>'.$getTrace[0]['function']. '</b> function. ' .$e->getTraceAsString();
                    exit();
                }

            }

        }

        return new self;

    }

    public  function get()
    {
        if(self::$select)
        {
            $select = 'SELECT ' . self::$select;
        } else {
            $select = 'SELECT *';
        }
        if(self::$table)
        {
            $form = ' FROM ' . self::$table;
        }
        else {

            try{
                throw new Exception("table method is missing. ");
            }catch(Exception $e){
                echo  $e->getMessage() . $e->getTraceAsString();
                exit();
            }
        }

        if( self::$where )
        {
            $where = ' WHERE ' . @self::$where['sql'];
        }

        $sql = $select.@$form.@$where.';' ;
        $result = QUERY::query($sql, @self::$where['value']);

        return $result;

    }

    public function count()
    {
        return count( json_decode(self::get()) );
    }



}
	

	
	