<?php

namespace sys\Database;

use sys\Database\Connection;
use PDO;

class QUERY extends Connection
{

    public function query($sql, $param = array()){
        //$this->_error = false;
//return $sql;
        $connect = new Connection;

        if($stmt = $connect->prepare($sql) ){

            if(count($param)){
                foreach ($param as $bind => $value ){
                    $stmt->bindValue($bind, $value);
                }
            }

            if ( $stmt->execute() ){
                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                return json_encode($result);
            }else {
               // $this->_error = true;
            }
        }
    }


}
	

	
	