<?php

namespace sys\Database;

use sys\Database\Connection;
use PDO;

class RAWQUERY extends Connection
{
	
	public function select($sql){
		$connect = new Connection;
		$stmt = $connect->prepare($sql);
		$stmt->execute();
		return (object) $stmt->fetchAll(PDO::FETCH_OBJ);
	}

	public function insert($sql){
		$connect = new Connection;
		$stmt = $connect->prepare($sql);
		$stmt->execute();
		$id = $connect->lastInsertId();
		return (object) compact('id');
		
	}
	
	public function update($sql){
		
		$connect = new Connection;
		$stmt = $connect->prepare($sql);
		$stmt->execute();
		return $stmt->rowCount();
		
	}
	
	public function delete($sql){
		
		$connect = new Connection;
		$stmt = $connect->prepare($sql);
		$stmt->execute();
		return $stmt->rowCount();
		
	}
	
	
	
	
}
	

	
	