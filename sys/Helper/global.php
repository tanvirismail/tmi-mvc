<?php
    use sys\Views;
    function view($page, $value=false ){

         Views::this($page, $value);

    }

    function baseDir()
    {
        $base = $_SERVER['SCRIPT_NAME'];
        $url = explode('/', $base);
        array_pop($url);
        $url = implode('/', $url);
        //$url = $_SERVER['HTTP_HOST'].$url;
        return $url;
    }
	
	function protocol()
	{
		$protocol = explode('/', $_SERVER["SERVER_PROTOCOL"]);
		$protocol = strtolower($protocol[0]).'://';
		return $protocol;
	}

	// form action
	function action($link)
	{
		return protocol().$_SERVER['HTTP_HOST'].baseDir().'/'.$link;
	}

	// html file link
	function assets($link)
	{
		return protocol().$_SERVER['HTTP_HOST'].baseDir().'/resource/assets/'.$link;
	}

    function filePath($path)
    {
        return baseDir().'/resource/storage/'.$path;
    }

    function redirect($page, $msg)
    {
        //$_SESSION['msg'] = $msg;
        header('Location:'.baseDir().$page);
    }

    function back()
    {
        header('Location:'.$_SERVER['REQUEST_URI']);
    }

    function csrf_token()
    {
        return sys\Http\Token::csrf_token();
    }


