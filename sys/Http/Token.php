<?php

namespace sys\Http;

use sys\Session\Session;

class Token
{

    public static function csrf_token()
	{
        $session = Session::getInstance();
        return $session->_token = md5(uniqid());

	}

    public static function csrf_token_check($token)
	{
		$tokenName = Config::get('session/token_name');

		if (Session::exists($tokenName) && $token === Session::get($tokenName)) {
			session::delete($tokenName);
			return true;
		}

		return false;
	}
}