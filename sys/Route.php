<?php
namespace sys;


class Route
{
	private static $passData = array();

	public static function get($route_url, $controller)
	{
		if( ($_SERVER['REQUEST_METHOD'] == 'GET') &&
			( !isset($_REQUEST['_method']) && @$_REQUEST['_method'] != 'PUT' && @$_REQUEST['_method'] != 'DELETE' ) )
		{
			if(isset($_GET) && !empty($_GET))
			{
				foreach($_GET as $k => $v){ self::$passData[ $k ] = $v; }
			}
			if( self::checkUrl($route_url) == 'home' && $route_url == '/' ){
                echo self::action($controller, self::$passData);
                exit();
			} 
			else {
				if( $route_url == self::checkUrl($route_url) )
				{
                    echo self::action($controller, self::$passData);
                    exit();
				}
                else { return false; }
			}
		}
		else { return false; }
	}
	
	public static function post($route_url, $controller)
	{
		if(($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST) && !empty($_POST) )
		{

//		    if(@$_POST['_token'] == '')
//            {
//                try{
//                    throw new Exception("csrf_token is not set ");
//                }catch(Exception $e){
//                    $getTrace = $e->getTrace();
//
//                    print_r($getTrace);
//
//                    //echo  $e->getMessage().' <b>'.$getTrace[0]['function']. '</b> function. ' .$e->getTraceAsString();
//                    exit();
//                }
//
//
//            }

			foreach($_POST as $k => $v)
			{ self::$passData[ $k ] = $v; }

			if(isset($_FILES) && !empty($_FILES))
			{ self::$passData[ 'file' ] = json_decode(json_encode($_FILES)); }

			if( $route_url == self::checkUrl($route_url) )
			{
                echo self::action($controller, self::$passData);
                exit();
			}
			else { return false; }
		}
		else { return false; }
	}
	
	public static function put($route_url, $controller)
	{
		if(isset($_REQUEST['_method']) && !empty($_REQUEST['_method']) && $_REQUEST['_method'] == 'PUT')
		{
			foreach($_REQUEST as $k => $v)
			{  self::$passData[ $k ] = $v; }

			if(isset($_FILES) && !empty($_FILES))
			{ self::$passData[ 'file' ] = json_decode(json_encode($_FILES)); }

			if( $route_url == self::checkUrl($route_url) )
			{
                echo self::action($controller, self::$passData);
                exit();
			}
			else { return false; }

		}
		else { return false; }
	}
	
	public static function delete($route_url, $controller)
	{
		if(isset($_REQUEST['_method']) && $_REQUEST['_method'] == 'DELETE') 
		{
			if( $route_url == self::checkUrl($route_url) )
			{
                echo self::action($controller, self::$passData);
                exit();
			}
			else { return false; }
		}
		else { return false; }
	}



    private static function checkUrl($route_url)
    {
        $indexPageLen = strlen($_SERVER['SCRIPT_NAME']);
        $getUrl = substr($_SERVER['PHP_SELF'], $indexPageLen);

        if( empty($getUrl) && $getUrl == '')
        {
            return 'home';
        }

        $urlArr = explode('/', $getUrl);
        $route_urlArr = explode('/', $route_url);

        if( end($urlArr) == ''){
            array_pop($urlArr);
        }

        $checkUrl = [];
        for( $i=0; $i<count($urlArr); $i++ )
        {
            if( preg_match('/\{*}/', @$route_urlArr[$i] ) == 1)
            {
                $checkUrl[ $route_urlArr[$i] ] = $route_urlArr[ $i ];
                $key = substr( $route_urlArr[ $i ], 1, -1 );
                self::$passData[ $key ] = $urlArr[ $i ] ;
            } else {
                $checkUrl[ @$route_urlArr[$i] ] = $urlArr[ $i ];
            }
        }
        return implode('/', $checkUrl);

    }

    private static function action($controller, $passData)
    {

        $control = rtrim(stristr($controller, '@', true), '@');
        $method = ltrim(stristr($controller, '@', false), '@');
        $class = "app\Controllers\\".$control;
        $instance = new $class;
        //$passData = json_encode((object) $passData);
        return $instance->$method( (object) @$passData );
    }


}







