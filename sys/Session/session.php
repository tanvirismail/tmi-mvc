<?php

namespace sys\Session;

class Session
{

    private $start = true;
    private $not_start = false;
    private $sessionStatus = false;

    private static $instance;

    private function __construct() {}

    public static function getInstance()
    {
        if ( !isset(self::$instance))
        {
            self::$instance = new self;
        }
        self::$instance->startSession();
        return self::$instance;
    }

    public function startSession()
    {
        if ( $this->sessionStatus == $this->not_start )
        {
            $this->sessionStatus = session_start();
        }
        return $this->sessionStatus;
    }
    
    public function __set( $name , $value )
    {
        $_SESSION[$name] = $value;
    }

    public function __get( $name )
    {
        if ( isset($_SESSION[$name]))
        {
            return $_SESSION[$name];
        }
    }

    public function __isset( $name )
    {
        return isset($_SESSION[$name]);
    }

    public function __unset( $name )
    {
        unset( $_SESSION[$name] );
    }

    public function destroy()
    {
        if ( $this->sessionStatus == $this->start )
        {
            $this->sessionStatus = !session_destroy();
            unset( $_SESSION );
            return !$this->sessionStatus;
        }
        
        return FALSE;
    }
}



//$data = Session::getInstance();
//
//
//$data->nickname = 'Someone';
//$data->age = 18;
//
//
//printf( '<p>My name is %s and I\'m %d years old.</p>' , $data->nickname , $data->age );
//
//printf( '<pre>%s</pre>' , print_r( $_SESSION , TRUE ));
//
//
//var_dump( isset( $data->nickname ));
//
//
////$data->destroy();
//
//echo "<br/>";
//echo $data->nickname ;

