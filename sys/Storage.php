<?php
namespace sys;

class Storage
{
	
	public function putFile($target, $file)
	{
		$target = self::target($target);
		$fileName = self::fileNaming($target, $file);
		
		$upload = move_uploaded_file($file->tmp_name, $target.$fileName);
		if($upload){
			return $fileName;
		} else {
			return '';
		}
	}
	
	public function saveAs($target, $rename, $file)
	{
		$target = self::target($target);
		
		$filePath = pathinfo($target.$file->name);
		$file->name = $rename . '.' . $filePath['extension'];
		
		$fileName = self::fileNaming($target, $file);
		
		$upload = move_uploaded_file($file->tmp_name, $target.$fileName);
		if($upload){
			return $fileName;
		} else {
			return '';
		}
	}
	
	public function save($target, $file)
	{
		$target = self::target($target);
		$upload = move_uploaded_file($file->tmp_name, $target.$file->name);
		if($upload){
			return $file->name;
		} else {
			return '';
		}
	}

	public function unLink($target, $fileName)
    {
        if($target == '/')
        {
            $target = FILE_Dir;
        } else {
            $target = FILE_Dir . $target .'/';
        }
        return unlink($target.$fileName);
    }

	public static function target($target)
	{
		if($target == '/')
		{
			return FILE_Dir;
		} else {
			return self::checkDir(FILE_Dir, $target ).'/';
		}
	}
		
	public static function checkDir($path, $dir) 
	{
		if(!is_dir($path.$dir) ){
		mkdir($path.$dir, 777, true);
		return $path.$dir;
		} else {
			return $path.$dir;
		}
	}
	
	public static function fileNaming($target, $file)
	{
		$next = '';
		$filePath = pathinfo($target.$file->name);
		for($i=0; $i<=500;)
		{
			$fileName = $filePath['filename'] . $next .'.'. $filePath['extension'];
			$destination = $target.$fileName;
			if( file_exists($destination) )
			{
				$i++; $next = '('.$i.')';
				$fileName = $filePath['filename'] . $next .'.'. $filePath['extension'];
				continue;
			} else {
				break;
			}
		}
		return $fileName;
	}
	

	
}





