<?php

namespace sys\Validation;

use sys\Database\DB;
use sys\Validation\Rules;

class Validator
{
    private $_passed = false,
    		$_errors = array();


	public function check($request, $rules)
	{
		foreach ($rules as $inputField => $rule)
		{
            $rule = explode('|', $rule);
            foreach($rule as $ruleValue)
            {
                $ruleValue = explode(':', $ruleValue);

                //Rules::$ruleValue[0]( $ruleValue[1] );

                switch ($ruleValue[0])       // min[0]:6[1]
                {
                    case 'required':
                        if($request->$inputField == '')
                        {
                            $this->addError($inputField, "{$inputField} is required");
                            break 2;
                        }
                        continue;
                    case 'min':
                        if( strlen( $request->$inputField ) < $ruleValue[1])
                        {
                            $this->addError($inputField, "{$inputField} must be a minimum of {$ruleValue[1]} characters.");
                            break 2;
                        }
                        continue;
                    case 'max':
                        if( strlen( $request->$inputField ) > $ruleValue[1] )
                        {
                            $this->addError($inputField, "{$inputField} must be a maximum of {$ruleValue[1]} characters.");
                            break 2;
                        }
                        continue;
                    case 'matches':
                        $matchField = $ruleValue[1];
                        if ( $request->$inputField != $request->$matchField ) {
                            $this->addError($inputField, "{$inputField} must be matched {$ruleValue[1]}");
                            break 2;
                        }
                        continue;
                    case 'unique':
                        $tbl = $ruleValue[1];
                        $check = DB::table($tbl)->where([$inputField, '=', $request->$inputField])->count();
                        if($check > 0)
                        {
                            $this->addError($inputField, "{$inputField} already exists");
                            break 2;
                        }
                        continue;
                    case 'email':
                        $email = $request->$inputField;

                        if(filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                            $this->addError($inputField, "Invalid {$inputField}");
                            break 2;
                        }
                        elseif(checkdnsrr('tmiweb.co', 'MX'))
                        {
                            $domain = ltrim(stristr($email, '@', false), '@');
                            $check = checkdnsrr($domain, 'MX');
                            if($check === false)
                            {
                                $this->addError($inputField, "Invalid {$inputField}");
                            }
                            break 2;
                        }



                }
			}
		}

        if (!empty($this->_errors)) {
            return false;
        } else {
            return true;
        }

	}

    private function addError($field, $message)
    {
        $this->_errors[$field] = $message;
    }

    public function errors()
    {
    	return $this->_errors;
    }

}